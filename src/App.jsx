import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from 'react';
import './App.css';
import Length from './Length';
import { AiOutlinePauseCircle,AiOutlinePlayCircle} from 'react-icons/ai'
import { MdOutlineAutorenew} from 'react-icons/md'

function App() {
  const [display, setDisplay] = useState(25 * 60);
 const [breakTime, SetBreakTime] = useState(5 * 60)
 const [sessionTime, SetSessionTime] = useState(25 * 60)
 const [timerOn, setTimerOn] = useState(false)
 const [onBreak, setOnBreak] = useState(false)
 const [onAudio, setOnAudio] = useState(new Audio("https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3"))
 const playSound=()=>{
      onAudio.currentTime=0
      onAudio.play()
 }

  const formateTime=(time)=>{
    let minute=Math.floor(time / 60);
    let second = time % 60
    return(
      (minute < 10 ? '0' + minute : minute ) + 
      ':' +  
      (second  < 10 ? '0' + second : second  )

    )
  
  }
  const changeTime= (amount,type)=>{
    if(breakTime <=60 && amount<0){
        return;
    }
    if(type === "break"){
      SetBreakTime(prev => prev + amount)
    }else{
      if(breakTime <=60 && amount<0){
         return;
      }
      SetSessionTime(prev => prev + amount)
      if(!timerOn){
          setDisplay(sessionTime + amount)
      }
    }

  }
  const control=()=>{
      let second = 1000;
      let data=new Date().getTime()
      let nexData=new Date().getTime() + second
      let onBreakVariable = onBreak
      if(!timerOn){
        let interval= setInterval(()=>{
          data=new Date().getTime()
          if(data > nexData){
            setDisplay(prev =>{
              if(prev <=0 && !onBreakVariable){
                playSound()
                onBreakVariable= true
                setOnBreak(true)
                return breakTime;
              }else if(prev <=0 && onBreakVariable){
                playSound()
                onBreakVariable= false
                setOnBreak(false)
                return sessionTime;

              }
              return prev - 1
            })
            nexData += second
          }
        },30)
        localStorage.clear()
        localStorage.setItem("interval-id", interval)
        
      };
      if(timerOn){
        clearInterval(localStorage.getItem("interval-id"));
      }
      setTimerOn(!timerOn)

  }
  const reset=()=>{
     setDisplay(25 * 60)
     SetBreakTime(5 * 60)
     SetSessionTime(25 * 60)
  }
  return (

    <div className="text-center">
      <h1>Pomodoro Clock</h1>
      <div className="dual-container">
      <Length title={"break length" } changeTime={changeTime} type={"break"} 
      time={breakTime} 
       formateTime={formateTime}
     />
       <Length title={"session length" } changeTime={changeTime} type={"session"} 
      time={sessionTime} 
       formateTime={formateTime}
     />
      </div>
      <h3>{onBreak ? 'break': "session"}</h3>
      <h1>{formateTime(display)}</h1>
      <button  className="btn-large btn btn-info lighten-2" onClick={control}>
           {
            timerOn ?  <AiOutlinePauseCircle  className='material-icons'/>:
            < AiOutlinePlayCircle className='material-icons'/>
           }
      </button>
      <button  className="btn-large btn btn-info lighten-2" onClick={control}>
          
            < MdOutlineAutorenew className='material-icons' onClick={reset}/>
         
      </button>
    </div>
  );
}

export default App;
