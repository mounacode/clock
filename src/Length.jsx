import { AiOutlineArrowDown, AiOutlineArrowUp} from 'react-icons/ai';

const Length = ({title,changeTime, type, time, formateTime}) => {
    
  return (
    <div>
      <h3>{title}</h3>
      <div className="time-sets">
        <button onClick={()=>changeTime(-60, type)} className="btn-small btn btn-info lighten-2">
           < AiOutlineArrowDown className='material-icons'/>
        </button>
        <h3>{formateTime(time)}</h3>
        <button onClick={()=>changeTime(60, type)}  className="btn-small btn btn-info lighten-2">
           < AiOutlineArrowUp className='material-icons'/>
        </button>
      </div>
    </div>
  )
}

export default Length
